﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class BattleManager : MonoBehaviour {

    public static BattleManager instance;

    public List<CharacterBattle> friendlyChara;
    public List<CharacterBattle> hostileChara;

    public List<CharacterBattle> chara;

    public BattleDebug debug;

    private void Awake()
    {
        if (instance == null)
            instance = this;
    }

    void BattleInit(List<CharacterBattle> _friendly, List<CharacterBattle> _hostile)
    {
        friendlyChara = _friendly;
        hostileChara = _hostile;

        chara = friendlyChara.Concat(hostileChara).ToList();
    }

    private void Start()
    {
        StartCoroutine(EnemyTurn());
    }


    IEnumerator EnemyTurn()
    {
        for(int i = 0; i < hostileChara.Count; i++)
        {
            while (hostileChara[i].q_command == null)
            {
                print("Select Command");
                hostileChara[i].c_control.SelectCommand(0);
                yield return new WaitForFixedUpdate();
            }

            while (hostileChara[i].q_target == null)
            {
                print("Select Target");
                hostileChara[i].c_control.SelectTarget(0);
                yield return new WaitForFixedUpdate();
            }

            yield return null;
        }

        yield return StartCoroutine(ExecuteCommands(0));
    }



    IEnumerator CommandUISetup(int index)
    {
        Commands[] array = chara[index].GetCommandList();


        yield return StartCoroutine(CommandInput(index));
    }

    IEnumerator CommandInput(int index)
    {


        //Wait for command input;
        while(chara[index].q_command == null)
        {

            yield return null;
        }

        //Wait for target input;
        while(chara[index].q_target == null)
        {

            yield return null;
        }

        if (index < chara.Count - 1)
            yield return StartCoroutine(CommandUISetup(index++)); //go to next character
        else
            yield return StartCoroutine(ExecuteCommands(0));//Execute Commands
    }

    public List<CharacterBattle> GetAvailableTarget(CharacterBattle actor)
    {
        if (actor.tag == "Player")
            return hostileChara;
        else return friendlyChara;
        
    }

    IEnumerator ExecuteCommands(int index)
    {
        hostileChara[index].ExecuteCommand();

        //onCommandExecutionFinished
        if (index < hostileChara.Count - 1) //if(index < chara.Count - 1)
            yield return StartCoroutine(ExecuteCommands(index++));
        else yield return null;
        //on All command Finished
        //yield return StartCoroutine(CommandInput(0));

        
    }

    void OnCharacterFallen(CharacterBattle character)
    {
        if (character.tag == "Enemy")
        {
            chara.Remove(character);
        }
        else
        {
            //set fainted
        }

    }

    void BattleEnd(bool isVictorious)
    {
        if (isVictorious)
            BattleWin();
        else
            BattleLose();
    }

    void BattleWin()
    {
        //tally exp gain
        //show battle summary
    }

    void BattleLose()
    {
        //game over screen
    }
}


