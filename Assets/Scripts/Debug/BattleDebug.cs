﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleDebug : MonoBehaviour
{

    public static BattleDebug instance;

    public Text textField;
    public ScrollRect scroll;
    public float yOffset = 16;
    public int lineCount;

    private void Awake()
    {
        if(instance == null)
            instance = this;
    }

    public void PrintLog(string text)
    {
        lineCount++;

        if (textField.rectTransform.sizeDelta.y < yOffset * lineCount)
        {
            textField.rectTransform.sizeDelta += new Vector2(0, yOffset);
            scroll.content.anchoredPosition += new Vector2(0, yOffset);
        }


        textField.text += (text + "\n");

    }
}
