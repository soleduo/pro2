﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleController {

    CharacterBattle b_chara;
    public List<CharacterBattle> availableTarget = new List<CharacterBattle>();

    public BattleController(CharacterBattle owner)
    {
        b_chara = owner;
    }

    public void SelectCommand(int index)
    {
        Commands comm = b_chara.c_commands[index];
        b_chara.OnCommandInput(comm);
    }

    public void SelectTarget(int index)
    {
        CharacterBattle target = availableTarget[index];
        b_chara.OnTargetInput(target);
    }
}
