﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public GameObject target;
    public Camera cam;

    public Transform m_pivot;

    public float distance;
    public float moveSpeed;
    public float zoomSpeed;

    public float minZoom = 10;
    public float maxZoom = 30;

    public bool lookAhead = false;
    [Range(0,10)]public float aheadDistance = 5f;


    Vector3 targetPos;



    // Update is called once per frame
    void FixedUpdate () {
        FollowTarget();

        distance = Mathf.Clamp(distance, minZoom, maxZoom);

        CameraZoom(distance);
	}

    void FollowTarget()
    {
        Vector3 velocity;



        if (!lookAhead) {
            velocity = Vector3.zero;
            targetPos = target.transform.position;
        }
        else
        {
            velocity = target.GetComponent<CharacterController>().velocity;
            targetPos = target.transform.position + velocity.normalized * aheadDistance;
        }
        transform.position = Vector3.SmoothDamp(transform.position, targetPos, ref velocity, Time.deltaTime * (moveSpeed), velocity.magnitude + moveSpeed);

        //transform.LookAt(target.transform.position);
    }

    public void CameraZoom(float d)
    {
        Vector3 zoomPos = Vector3.forward * d * -1;

        cam.transform.localPosition = Vector3.Lerp(cam.transform.localPosition, zoomPos, Time.deltaTime);
    }

    public IEnumerator RotatePivot(Quaternion target)
    {

        while (m_pivot.localRotation != target)
        {
            m_pivot.localRotation = Quaternion.Slerp(m_pivot.rotation, target, Time.deltaTime);

            yield return null;
        }
        yield return null;
    }

    public void RotatePivot(Quaternion startRot, Quaternion target, float t) // t is progress between 0 to 1
    {
        
            //print(startRot);

            m_pivot.localRotation = Quaternion.Slerp(startRot, target, t);
            //print(Quaternion.Slerp(startRot, rot, t));
            
        
        
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(targetPos, 1);
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, 1);

        Gizmos.color = Color.white;
        Gizmos.DrawLine(targetPos, transform.position);
    }
}
