﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Commands {

    //Identifier
    public int id;
    public string c_name;

    public float value;


    public void OnCommandExecute(CharacterBattle target)
    {
        target.SetHealth(value);
    }

}

