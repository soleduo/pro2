﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterBattle : MonoBehaviour {

    public BattleController c_control;

    public Commands[] c_commands;
    

    [HideInInspector]public Commands q_command = null;
    [HideInInspector]public CharacterBattle q_target = null;

    public float health;

    private void Awake()
    {
        c_control = new BattleController(this);

        q_command = null;
        q_target = null;
    }

    public Commands[] GetCommandList()
    {
        return c_commands;
    }

    public void OnCommandInput(Commands comm)
    {
        

        print("Command Set");

        c_control.availableTarget = BattleManager.instance.GetAvailableTarget(this);

        q_command = comm;
    }

    public void OnTargetInput(CharacterBattle target)
    {
        q_target = target;
    }

    public void ExecuteCommand()
    {
        BattleDebug.instance.PrintLog(name + " used " + q_command.c_name + ".");

        //Apply Command on Target
        
        q_command.OnCommandExecute(q_target);
        //BattleDebug.instance.PrintLog(name + " used " + q_command.c_name);
        

        //reset queued Command;
        q_command = null;
        q_target = null;
    }

    public void SetHealth(float value)
    {
        
        health -= value;
        BattleDebug.instance.PrintLog(name + " hit for " + value + " health.");
        
  
    }

    public void StatChange(string stat, float value)
    {
        
    }
}
