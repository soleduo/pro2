﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class Character : MonoBehaviour {

    [SerializeField]private Controller m_controller;
    [SerializeField]private CharacterController c_controller;

    [SerializeField] private Animator m_animator;
    private bool isAttack;

    CharacterData data;

    [Header("Look Axes")]
    [SerializeField]
    private bool x;
    [SerializeField]
    private bool z;

    private Vector3 lookAxes;

    void Awake()
    {
        data = new CharacterData();
    }

    private void Start()
    {
        if (x)
            lookAxes += Vector3.right;
        if (z)
            lookAxes += Vector3.forward;

        lookAxes += Vector3.up;
    }

    public void Movement(Vector3 inputDir)
    {
        if (inputDir.magnitude > 1f) inputDir.Normalize();

        Vector3 moveDir = inputDir * data.moveSpeed;

        AnimatorUpdate(inputDir.magnitude);

        c_controller.SimpleMove(moveDir);

        Vector3 lookAtPos = transform.position + Vector3.Scale(inputDir, lookAxes);

        transform.LookAt(lookAtPos);
    }

    public void Attack()
    {

    }

    void AnimatorUpdate(float m_speed)
    {
        m_animator.SetFloat("speed", m_speed, 0.1f, Time.deltaTime);
    }
}
