﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Character))]
public class PlayerController : Controller {

    private Character chara;
    [SerializeField] CameraFollow m_CamFollow;
    private Transform cam;

    private Vector3 camForward;

    [SerializeField] private bool useCamera;

    [Header("Movement Axes")]
    [SerializeField] private bool x = true;
    [SerializeField] private bool z = true;
    private Vector3 moveAxes;

    private void Start()
    {
        if (Camera.main != null)
            cam = Camera.main.transform;

        if (x)
            moveAxes += Vector3.right;
        if (z)
            moveAxes += Vector3.forward;

        chara = GetComponent<Character>();
    }

    public void SetUseCamera(bool useCam)
    {
        useCamera = useCam;
    }

    private void FixedUpdate()
    {

        OnMovementInput();
        CameraZoomInput();

        if (Input.GetButtonDown("Interact"))
            OnInteractInput();
    }


    void OnMovementInput()
    {
        // read inputs
        float h = Input.GetAxis("Horizontal") * moveAxes.x;
        float v = Input.GetAxis("Vertical") * moveAxes.z;

        // calculate move direction to pass to character
        if (cam != null && useCamera)
        {
            // calculate camera relative direction to move:
            camForward = Vector3.Scale(cam.forward, new Vector3(1, 0, 1)).normalized;
            inputDir = v * camForward + h * cam.right;
        }
        else
        {
            // we use world-relative directions in the case of no main camera
            inputDir = v * Vector3.forward + h * Vector3.right;
        }

        chara.Movement(inputDir);
    }

    void OnInteractInput()
    {
        print("Try Interact");

        Vector3 fwd = transform.TransformDirection(Vector3.forward);

        RaycastHit hit;

        Debug.DrawRay(transform.position, fwd * 1.5f, Color.red, 3f);

        if (Physics.Raycast(transform.position, fwd, out hit, 1.5f))
        {
            if (hit.collider.tag == "Interactibles")
            {
                hit.collider.GetComponentInParent<Interactibles>().OnInteract();
            }
        }
    }

    void CameraZoomInput()
    {
        float d = Input.GetAxis("Mouse ScrollWheel");

        m_CamFollow.distance += d * 10 * m_CamFollow.zoomSpeed;
    }
}
