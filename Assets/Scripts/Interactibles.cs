﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Interactibles : MonoBehaviour {

    public abstract void OnInteract();
}
