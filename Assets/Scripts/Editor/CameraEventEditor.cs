﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using CameraEvent;

[CustomEditor(typeof(CameraEventTrigger))]
public class CameraEventEditor : Editor {

    private int index;
    private CameraFollow camFollow;

    private string[] modeName = new string[] { "Rotate", "Zoom", "Focus" };

    public override void OnInspectorGUI()
    {
        CameraEventTrigger script = (CameraEventTrigger)target;

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Event Mode", GUILayout.Width(126));
        index = EditorGUILayout.Popup(index, modeName, GUILayout.Width(126));
        EditorGUILayout.EndHorizontal();

        script.m_type = (CameraEventType)index;

        camFollow = Camera.main.GetComponentInParent<CameraFollow>();
        Debug.Log(camFollow);

        if(index == 0)
        {
            EditorGUILayout.BeginHorizontal();
            script.start = EditorGUILayout.Vector3Field("Start Rotation", script.start);
            if (GUILayout.Button("Copy Camera Rotation"))
                script.start = camFollow.m_pivot.rotation.eulerAngles;
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            script.end = EditorGUILayout.Vector3Field("End Rotation", script.end);
            if (GUILayout.Button("Copy Camera Rotation"))
                script.end = camFollow.m_pivot.rotation.eulerAngles;
            EditorGUILayout.EndHorizontal();
        }

        EditorGUILayout.BeginHorizontal();
        script.isContinuous = EditorGUILayout.ToggleLeft("Continuous", script.isContinuous);
        script.useCamera = EditorGUILayout.ToggleLeft("Use Camera", script.useCamera);
        EditorGUILayout.EndHorizontal();
    }
}
