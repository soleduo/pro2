﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CameraEvent {
    [RequireComponent(typeof(Collider))]
    public class CameraEventTrigger : MonoBehaviour {

        private Collider m_collider;
        private CameraFollow m_camFollow;
        public CameraEventType m_type;
        private int traceDir = 0;
        

        public Vector3 start;
        public Vector3 end;

        private Quaternion startRot;
        private Quaternion endRot;

        float targetPos;
        float currentPos;

        float progress;

        public bool isContinuous;
        public bool useCamera; //used to change player movement input pivot to camera relative

        private void Awake()
        {
            if(m_collider == null)
                m_collider = GetComponent<Collider>();

        }

        // Use this for initialization
        void Start () {
            if (m_camFollow == null)
                m_camFollow = Camera.main.GetComponentInParent<CameraFollow>();	

            if(m_type == CameraEventType.Rotate)
            {
                startRot = Quaternion.Euler(start);
                endRot = Quaternion.Euler(end);
            }
	    }

        private void OnTriggerEnter(Collider other)
        {
            if (!isContinuous)
            {
                if (other.tag == "Player")
                {
                    StartCoroutine(m_camFollow.RotatePivot(endRot));
                    other.GetComponent<PlayerController>().SetUseCamera(useCamera);
                }
            }
        }

        private void OnTriggerStay(Collider other)
        {
            if (isContinuous)
            {


                if (other.tag == "Player")
                {
                    switch (traceDir)
                    {
                        case 0:
                            targetPos = m_collider.bounds.max.x - m_collider.bounds.min.x;
                            currentPos = other.transform.position.x - m_collider.bounds.min.x;



                            break;

                        case 1:
                            targetPos = m_collider.bounds.max.y - m_collider.bounds.min.y;
                            currentPos = other.transform.position.y - m_collider.bounds.min.y; 

                            break;

                        case 2:
                            targetPos = m_collider.bounds.max.z - m_collider.bounds.min.z;
                            currentPos = other.transform.position.z - m_collider.bounds.min.z;

                            break;
                    }

                    progress = Mathf.Clamp(currentPos / targetPos, 0, 1);

                    //print(progress);

                    //print(m_camStartRot);
                    m_camFollow.RotatePivot(startRot, endRot, progress);

                    other.GetComponent<PlayerController>().SetUseCamera(useCamera);
                }
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if(isContinuous)
            {
                if(other.tag == "Player")
                {
                    if (progress >= .9f)
                        StartCoroutine(m_camFollow.RotatePivot(endRot));
                    else
                        StartCoroutine(m_camFollow.RotatePivot(startRot));
                    other.GetComponent<PlayerController>().SetUseCamera(useCamera);
                }
            }
        }

        private void OnDrawGizmosSelected()
        {
            
            if(m_collider == null)
                m_collider = GetComponent<Collider>();
            else
                Gizmos.DrawWireCube(m_collider.bounds.center, m_collider.bounds.size);
        }
    }

    public enum CameraEventType
    {
        Rotate,
        Zoom,
        SetFocus
    }
}